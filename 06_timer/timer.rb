class Timer
  def initialize
    @seconds = 0
  end

  def seconds
    @seconds
  end

  def seconds=(value)
    @seconds = value
  end

  def time_string
    #get seconds divide it into minutes and hours and format it
    hours = 0
    minutes = 0
    seconds = @seconds

    if seconds > 3600
      hours = seconds/3600
    end

    seconds -= (hours*3600)

    if seconds > 60
      minutes = seconds/60
    end

    seconds -= minutes * 60

    time_string = "#{padded hours}:#{padded minutes}:#{padded seconds}"
  end

  private 

  def padded number
    "%02i" % number
  end

end
