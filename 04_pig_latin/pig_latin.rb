def translate phrase
    words = phrase.split
    final_phrase = ""
    words.each do |word|
        final_phrase += (translate_word word) + " "
    end
    final_phrase.strip
end

def translate_word word
    #check and remove punctuation...
    punctuation_index = get_punctuation_index word
    unless punctuation_index == nil
        removed_punctuation = remove_punctuation word
    else
        punctuation_index = 0
    end
    
    #...put our word together...
    final_word = ""
    if vowel? word[0..0]
        final_word += word + "ay"
    else
        if uppercase? word[0..0]
            final_word += (move_consonant_phoneme_back word).downcase.capitalize + "ay"
        else
            final_word += (move_consonant_phoneme_back word) + "ay"
        end
    end
    #...re-add any removed punctuation
    unless removed_punctuation == nil 
        if punctuation_index == word.length #factor in removed punctuation and added "ay"
            final_word.insert(final_word.length, removed_punctuation)
        else
            final_word.insert(final_word.length-punctuation_index, removed_punctuation)
        end
    end
    return final_word
end


def vowel? letter
    return letter.match? /[aeiou]{1}/i
end

def move_consonant_phoneme_back word
    final_word = ""
    phonmeme_length = get_starting_phoneme_length word
    consonant_length = get_starting_consonant_length word

    if phonmeme_length > consonant_length
        desired_substring_length = phonmeme_length
    else
        desired_substring_length = consonant_length
    end
    
    final_word = word[desired_substring_length+1..-1] + word[0..desired_substring_length]

    return final_word
end

def get_starting_phoneme_length word
    if word.length < 2
        return 0
    else
        if word.start_with?(/qu/i)
            return 1
        elsif word.start_with?(/[a-z]{1}qu/i)
            return 2
        end
        return 0
    end
end

def get_starting_consonant_length word
    last_consonant_index = 0
    while !vowel? word[last_consonant_index]
        last_consonant_index += 1
    end
   return last_consonant_index-1
end

def uppercase? letter
  return letter.match? /[A-Z]{1}/
end

def get_punctuation_index word
    return word.index(/\W/)
end

def remove_punctuation word
    removed = word.slice(/\W/)
    word.slice!(/\W/)
    return removed
end