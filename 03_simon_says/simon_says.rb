def echo phrase
    phrase
end

def shout phrase
    phrase.upcase
end

def repeat *phrase
    if phrase.length == 1
        final_phrase = "#{phrase.first} #{phrase.first}"
    else
        final_phrase = "#{(phrase.first + " ") * phrase.last}".strip
    end
end

def start_of_word word,number_of_letters 
    word[0..(number_of_letters-1)]
end

def first_word phrase
    words = phrase.split()
    words.first
end

def titleize phrase
    words = phrase.split()
    final_phrase = ""
    words.each do |word|
        unless (word == "the" || word == "and" || word == "over") && word.object_id != words.first.object_id
            final_phrase += word.capitalize
        else
            final_phrase += word
        end
        final_phrase += " "
    end
    final_phrase.strip
end