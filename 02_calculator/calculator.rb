def add number_1, number_2
    sum = number_1 + number_2
end

def subtract number_1, number_2
    sum = number_1 - number_2
end

def sum numbers
    sum = 0
    numbers.each do |number|
        sum += number
    end
    sum
end

def multiply numbers
    sum = 1
    numbers.each do |number|
        sum *= number
    end
    sum
end

def power number_1, number_2
    sum = number_1 ** number_2
end

def factorial number
    if number == 0
        return 1
    end
    
    sum = 1
    while number > 0 
        sum *= number
        number -= 1
    end
    sum
end