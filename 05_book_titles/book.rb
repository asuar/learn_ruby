class Book
    def initialize 
        @title = ""
    end

    def title=(phrase)
        @title = capitalize phrase
    end

    def title
        @title
    end

 private
    def capitalize phrase
        final_phrase = ""
        words = phrase.split
        words.each do |word|
            if should_capitalize? word
                final_phrase += word.capitalize + " "
            else
                if final_phrase == "" #always capitalize the first word
                    final_phrase += word.capitalize + " "
                else
                    final_phrase += word + " "
                end

            end
        end
        final_phrase.strip
    end

    def should_capitalize? word
        unless word.match?(/\A(and)\z|\A(the)\z|\A(an)\z|\A(of)\z|\A(in)\z|\A(a)\z/i)
            return true
        end
        false
    end
end
