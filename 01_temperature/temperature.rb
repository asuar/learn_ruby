def ftoc temperature_f
    temperature_c = ((temperature_f - 32) * 5/9.0)
end

def ctof temperature_c
    temperature_f = (temperature_c * 9/5.0) + 32
end